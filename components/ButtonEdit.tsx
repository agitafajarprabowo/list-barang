import {
	Button,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
	useToast,
	Text,
	Box,
	Flex,
	FormControl,
	FormLabel,
	Wrap,
	WrapItem,
	Input,
	ModalCloseButton,
} from '@chakra-ui/react';
import { BiEdit } from 'react-icons/bi';
import { BsCloudUploadFill } from 'react-icons/bs';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';
import Dropzone from 'react-dropzone';
import { ListBarang } from '../interfaces';
import axios from 'axios';
import React from 'react';
import router from 'next/router';
import { useRefetchListBarang } from '../http/hooks/useRefetchListBarang';

interface EditListBarangProps {
	id: number;
	listData?: ListBarang;
}

const listBarangFormSchema = z.object({
	foto: z.string().min(1, { message: 'Foto harus diisi' }),
	nama: z.string().min(1, { message: 'Nama harus diisi' }),
	harga_beli: z.string().min(1, { message: 'Harga beli harus diisi' }),
	harga_jual: z.string().min(1, { message: 'Harga jual harus diisi' }),
	stock: z.string().min(1, { message: 'Stock harus diisi' }),
});

type ListBarangFormData = z.infer<typeof listBarangFormSchema>;

const getDefaultValues = (listData?: ListBarang) => {
	return {
		nama: listData?.nama ?? '',
		foto: listData?.foto ?? '',
		harga_beli: listData?.harga_beli ?? '',
		harga_jual: listData?.harga_jual ?? '',
		stock: listData?.stock ?? '',
	};
};

const ButtonEdit: React.FC<EditListBarangProps> = ({ id, listData }) => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const toast = useToast();
	const [selectedFile, setSelectedFile] = React.useState<File | null>(null);
	const defaultValues = getDefaultValues(listData);
	const { refetchListBarangView } = useRefetchListBarang();

	const handleFileSelect = (files: File[]) => {
		const file = files[0];
		if (file) {
			const fileName = file.name;
			const fileSize = file.size / 1024; // Convert to KB
			const fileExtension = fileName.split('.').pop()?.toLowerCase();
			const allowedExtensions = ['jpg', 'jpeg', 'png'];
			if (!allowedExtensions.includes(fileExtension)) {
				alert(
					'Format foto tidak valid. Hanya file JPG dan PNG yang diperbolehkan.'
				);
				return;
			}

			if (fileSize > 100) {
				alert('Ukuran foto melebihi batas maksimal 100KB.');
				return;
			}

			setSelectedFile(file);
			setValue('foto', file.name); // Set the value of the "foto" field to the selected file
		}
	};

	const {
		control,
		register,
		handleSubmit,
		reset,
		setValue,
		formState: { errors },
	} = useForm<ListBarangFormData>({
		resolver: zodResolver(listBarangFormSchema),
		defaultValues,
	});

	const handleSubmits = async (e: any) => {
		e.preventDefault();

		const formData = new FormData(e.target);
		formData.append('id', id.toString());

		try {
			await fetch(`/api/products?id=${id}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(Object.fromEntries(formData)),
			});
			console.log('Data berhasil diperbarui');
			toast({
				title: 'Data berhasil diperbarui',
				description: `Data berhasil diperbarui.`,
				status: 'success',
				duration: 3000,
				isClosable: true,
				position: 'top-right',
			});
			refetchListBarangView();
			onClose();
		} catch (error) {
			console.log('Gagal memperbarui data', error);
		}
	};

	return (
		<>
			<Box
				onClick={onOpen}
				bg="#32BC65"
				display="flex"
				justifyItems="center"
				alignItems="center"
				rounded="xl"
				mr="2"
				cursor="pointer"
			>
				<Icon color="#fff" as={BiEdit} w={4} h={4} m="2" />
			</Box>
			<Modal isOpen={isOpen} onClose={onClose} size="3xl" isCentered>
				<ModalOverlay />
				<ModalContent px="12" py="2" rounded="xl">
					<form onSubmit={handleSubmits}>
						<ModalHeader display="flex">
							<Text textAlign="left" fontWeight="bold" fontSize="2xl">
								Edit Barang
							</Text>
							<ModalCloseButton />
						</ModalHeader>
						<ModalBody fontSize="lg" mt="4" textAlign="center">
							<Wrap justify="center" spacing="8px" spacingY="4">
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Nama Barang</FormLabel>
										<Input
											type="text"
											{...register('nama', { required: 'Nama harus diisi' })}
										/>
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Stock</FormLabel>
										<Input
											type="number"
											{...register('stock', { required: 'Stock harus diisi' })}
										/>
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Harga Beli</FormLabel>
										<Input
											type="number"
											{...register('harga_beli', {
												required: 'Harga beli harus diisi',
											})}
										/>
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Harga Jual</FormLabel>
										<Input
											type="number"
											{...register('harga_jual', {
												required: 'Harga jual harus diisi',
											})}
										/>
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel textAlign="center">Foto</FormLabel>
										<Dropzone onDrop={handleFileSelect}>
											{({ getRootProps, getInputProps }) => (
												<Flex
													{...getRootProps()}
													direction="column"
													alignItems="center"
													p={4}
													borderWidth={2}
													borderRadius="lg"
													borderColor="gray.300"
													cursor="pointer"
												>
													<input {...getInputProps()} />
													<Icon
														as={BsCloudUploadFill}
														boxSize={8}
														color="gray.500"
													/>
													<Text mt={2} color="gray.600">
														{selectedFile ? selectedFile.name : 'Pilih file'}
													</Text>
												</Flex>
											)}
										</Dropzone>
									</FormControl>
								</WrapItem>
							</Wrap>
						</ModalBody>
						<ModalFooter display="flex" justifyContent="center">
							<Button
								variant="ghost"
								mr={6}
								onClick={onClose}
								rounded="lg"
								bg="#fff"
								w="50%"
								h="50px"
								fontSize="lg"
								color="#3A8869"
								borderWidth="2px"
								borderColor="#3A8869"
							>
								Batal
							</Button>
							<Button
								type="submit"
								variant="ghost"
								mr={3}
								rounded="lg"
								bg="#3A8869"
								w="50%"
								h="50px"
								fontSize="lg"
								color="#fff"
								onClick={onClose}
								_hover={{ bg: '#307759' }}
							>
								Simpan
							</Button>
						</ModalFooter>
					</form>
				</ModalContent>
			</Modal>
		</>
	);
};

export default ButtonEdit;
