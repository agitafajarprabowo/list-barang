import {
	Button,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
	useToast,
	Text,
} from '@chakra-ui/react';
import router from 'next/router';
import { destroyCookie } from 'nookies';
import * as React from 'react';
import { IoWarningOutline } from 'react-icons/io5';
import { getSuccessToastConfigs } from '../lib/toastConfigs';
import { BiLogOutCircle } from 'react-icons/bi';

const ButtonLogout: React.FC = () => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const toast = useToast();

	const handleLogout = () => {
		destroyCookie(null, 'token');
		toast(
			getSuccessToastConfigs({
				title: 'Berhasil',
				description: 'Keluar',
			})
		);
		router.push('/login');
	};

	return (
		<>
			<Button w="full" bg="#DFF1EA" _hover={{ bg: '#DFF1EA' }} onClick={onOpen}>
				<Icon
					color="#388466"
					as={BiLogOutCircle}
					w={4}
					h={4}
					cursor="pointer"
					mr="2"
				/>
				<Text color="#388466">Keluar</Text>
			</Button>
			<Modal isOpen={isOpen} onClose={onClose} size="lg" isCentered>
				<ModalOverlay />
				<ModalContent px="12" py="2" rounded="xl">
					<ModalHeader
						justifyContent="center"
						display="flex"
						color="#ED4C5C"
						mt="2"
					>
						<Icon as={IoWarningOutline} w="20" h="20" color="#3A8869" />
					</ModalHeader>
					<ModalBody fontSize="lg" mb="10" mt="6" textAlign="center">
						<Text fontWeight="bold" fontSize="xl" mb="2">
							Konfirmasi
						</Text>
						<Text> Anda yakin ingin keluar ?</Text>
					</ModalBody>
					<ModalFooter display="flex" justifyContent="center">
						<Button
							variant="ghost"
							mr={6}
							onClick={onClose}
							rounded="lg"
							bg="#F4F4F4"
							w="50%"
							h="50px"
							fontSize="lg"
							color="#4A4A4A"
						>
							Batal
						</Button>
						<Button
							variant="ghost"
							mr={3}
							rounded="lg"
							bg="#3A8869"
							w="50%"
							h="50px"
							fontSize="lg"
							color="white"
							_hover={{ bg: '#3A8869' }}
							onClick={handleLogout}
						>
							Delete
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};

export default ButtonLogout;
