import {
	Avatar,
	Box,
	Button,
	Container,
	Flex,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Stack,
	Text,
	useDisclosure,
	useToast,
} from '@chakra-ui/react';
import Head from 'next/head';
import Link from 'next/link';
import React, { ReactNode } from 'react';
import router from 'next/router';
import { destroyCookie } from 'nookies';
import { BiLogOutCircle } from 'react-icons/bi';
import { IoWarningOutline } from 'react-icons/io5';
import { getSuccessToastConfigs } from '../lib/toastConfigs';

type BaseLayoutProps = {
	children: ReactNode;
	pageTitle: string;
};

const BaseLayout = ({ children, pageTitle }: BaseLayoutProps) => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const toast = useToast();

	const handleLogout = () => {
		destroyCookie(null, 'token');
		toast(
			getSuccessToastConfigs({
				title: 'Berhasil',
				description: 'Keluar',
			})
		);
		router.push('/login');
	};
	return (
		<>
			<Head>
				<title>{pageTitle}</title>
				<meta charSet="utf-8" />
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Flex h="100vh" bg="#FAFCFE">
				<Box
					w="250px"
					bg="#fff"
					color="#7B8794"
					borderRightWidth="2px"
					display="flex"
					justifyContent="space-between"
					position="fixed"
					h="full"
				>
					<Flex p="6" flexDir="column" justifyContent="space-between" w="full">
						<Box>
							<Text fontSize="3xl" fontWeight="bold" mb="12" align="center">
								The Menu
							</Text>
							<Text mb="4" fontWeight="bold" fontSize="sm">
								Main Menu
							</Text>
							<Link href="/" style={{ textDecoration: 'none' }}>
								<Text
									pb="6"
									_hover={{ textDecoration: 'none' }}
									borderBottomWidth="2px"
									color="#3A8769"
									fontWeight="bold"
								>
									Dashboard
								</Text>
							</Link>
						</Box>

						<Box>
							<Flex align="center" borderBottomWidth="2px" mb="6" pb="6">
								<Avatar bg="teal.500" size="sm" mr="4" />
								<Stack fontSize="sm" spacing="-1">
									<Text>Admin</Text>
									<Text>Mr. Admin</Text>
								</Stack>
							</Flex>
							<Button
								w="full"
								bg="#DFF1EA"
								_hover={{ bg: '#DFF1EA' }}
								onClick={onOpen}
							>
								<Icon
									color="#388466"
									as={BiLogOutCircle}
									w={4}
									h={4}
									cursor="pointer"
									mr="2"
								/>
								<Text color="#388466">Keluar</Text>
							</Button>
							<Modal isOpen={isOpen} onClose={onClose} size="lg" isCentered>
								<ModalOverlay />
								<ModalContent px="12" py="2" rounded="xl">
									<ModalHeader
										justifyContent="center"
										display="flex"
										color="#ED4C5C"
										mt="2"
									>
										<Icon as={IoWarningOutline} w="20" h="20" color="#3A8869" />
									</ModalHeader>
									<ModalBody fontSize="lg" mb="10" mt="6" textAlign="center">
										<Text fontWeight="bold" fontSize="xl" mb="2">
											Konfirmasi
										</Text>
										<Text> Anda yakin ingin keluar ?</Text>
									</ModalBody>
									<ModalFooter display="flex" justifyContent="center">
										<Button
											variant="ghost"
											mr={6}
											onClick={onClose}
											rounded="lg"
											bg="#F4F4F4"
											w="50%"
											h="50px"
											fontSize="lg"
											color="#4A4A4A"
										>
											Batal
										</Button>
										<Button
											variant="ghost"
											mr={3}
											rounded="lg"
											bg="#3A8869"
											w="50%"
											h="50px"
											fontSize="lg"
											color="white"
											_hover={{ bg: '#3A8869' }}
											onClick={handleLogout}
										>
											Delete
										</Button>
									</ModalFooter>
								</ModalContent>
							</Modal>
						</Box>
					</Flex>
				</Box>
				<Flex ml="230px" flex="1" flexDirection="column">
					<Flex bg="#3A8869" h="100px" px="8" py="2" align="center">
						<Text color="white" fontSize="2xl" fontWeight="bold">
							My Apps
						</Text>
					</Flex>
					<Container maxW="6xl" flex="1" px="8" py="6" bg="#FAFCFE">
						{children}
					</Container>
				</Flex>
			</Flex>
		</>
	);
};

export default BaseLayout;
