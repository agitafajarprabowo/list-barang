import {
	Icon,
	Input,
	InputGroup,
	InputLeftElement,
	Box,
} from '@chakra-ui/react';
import * as React from 'react';
import { BiSearch } from 'react-icons/bi';

interface SearchFieldProps {
	onSearch: (searchTerm: string) => void;
}

const SearchField: React.FC<SearchFieldProps> = ({ onSearch }) => {
	const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
		const searchTerm = event.target.value;
		onSearch(searchTerm); // Call the onSearch callback with the searchTerm
	};

	return (
		<Box w="350px">
			<InputGroup>
				<InputLeftElement pointerEvents="none">
					<Icon as={BiSearch} color="gray.300" />
				</InputLeftElement>
				<Input
					type="tel"
					placeholder="Cari nama barang ..."
					onChange={handleSearch}
				/>
			</InputGroup>
		</Box>
	);
};

export default SearchField;
