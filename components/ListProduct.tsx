import {
	Grid,
	GridItem,
	Flex,
	Box,
	Image,
	useDisclosure,
} from '@chakra-ui/react';
import * as React from 'react';
import ButtonEdit from './ButtonEdit';
import ButtonHapus from './ButtonHapus';
import { ListBarang } from '../interfaces';

export interface getAllListBarangProps {
	dataList: ListBarang[];
}

const ListProduct: React.FC<getAllListBarangProps> = ({ dataList }) => {
	const { isOpen, onOpen, onClose } = useDisclosure();

	return (
		<>
			{dataList?.map((itemlist, num) => {
				return (
					<Grid
						gridTemplateColumns="min-content 1.2fr 1fr 1fr 1fr 1fr 0.8fr"
						p="4"
						borderWidth="2px"
						_hover={{ bg: '#DFF1EA' }}
						key={itemlist.id}
					>
						<GridItem mr="6">{num + 1}</GridItem>
						<GridItem>
							{typeof itemlist.foto === 'string' ? (
								<Box>{itemlist.foto}</Box>
							) : (
								<Image
									src={URL.createObjectURL(itemlist.foto)}
									alt="Foto Barang"
								/>
							)}
						</GridItem>
						<GridItem textAlign="center">{itemlist.nama}</GridItem>
						<GridItem textAlign="center">{itemlist.harga_jual}</GridItem>
						<GridItem textAlign="center">{itemlist.harga_beli}</GridItem>
						<GridItem textAlign="center">{itemlist.stock}</GridItem>
						<GridItem>
							<Flex w="full" justify="center">
								<ButtonEdit id={itemlist.id} listData={itemlist} />

								<ButtonHapus id={itemlist.id} nama={itemlist.nama} />
							</Flex>
						</GridItem>
					</Grid>
				);
			})}
		</>
	);
};

export default ListProduct;
