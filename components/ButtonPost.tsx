import {
	Button,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
	Text,
	ModalCloseButton,
	FormControl,
	FormLabel,
	Input,
	Wrap,
	WrapItem,
	Flex,
	Grid,
	GridItem,
} from '@chakra-ui/react';
import * as React from 'react';
import { BiPlus } from 'react-icons/bi';
import { BsCloudUploadFill } from 'react-icons/bs';
import { ListBarang } from '../interfaces';
import { useForm } from 'react-hook-form';
import Dropzone from 'react-dropzone';

interface ButtonPostProps {
	mutateAsync: (listBarang: ListBarang) => void;
	refetchAsync?: () => void;
}

const ButtonPost: React.FC<ButtonPostProps> = ({
	mutateAsync,
	refetchAsync,
}) => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const {
		register,
		handleSubmit,
		reset,
		formState: { errors },
	} = useForm<ListBarang>();
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const [nameFile, setNameFile] = React.useState<string>('');
	const [selectedFile, setSelectedFile] = React.useState<File | null>(null);

	const onFormSubmit = async (data: ListBarang) => {
		try {
			setIsLoading(true);
			data.foto = selectedFile.name;
			await mutateAsync(data);
			await refetchAsync();
			reset();
			setSelectedFile(null);
			onClose();
		} catch (error) {
			console.log(error);
		} finally {
			setIsLoading(false);
			reset();
		}
	};

	const handleFileSelect = (files: File[]) => {
		const file = files[0];
		if (file) {
			const fileName = file.name;
			const fileSize = file.size / 1024; // Convert to KB
			const fileExtension = fileName.split('.').pop()?.toLowerCase();
			const allowedExtensions = ['jpg', 'jpeg', 'png'];

			if (!allowedExtensions.includes(fileExtension)) {
				alert(
					'Format foto tidak valid. Hanya file JPG dan PNG yang diperbolehkan.'
				);
				return;
			}

			if (fileSize > 100) {
				alert('Ukuran foto melebihi batas maksimal 100KB.');
				return;
			}

			setSelectedFile(file);
			setNameFile(fileName);
		}
	};

	return (
		<>
			<Button
				bg="#3A8769"
				_hover={{ bg: '#3A8769' }}
				onClick={onOpen}
				px="10"
				rounded="xl"
			>
				<Icon color="#fff" as={BiPlus} w={4} h={4} cursor="pointer" mr="2" />
				<Text color="#fff">Tambah Kategori</Text>
			</Button>
			<Modal isOpen={isOpen} onClose={onClose} size="3xl" isCentered>
				<ModalOverlay />
				<ModalContent px="12" py="2" rounded="xl">
					<form onSubmit={handleSubmit(onFormSubmit)}>
						<ModalHeader display="flex">
							<Text textAlign="left" fontWeight="bold" fontSize="2xl">
								Tambah Barang
							</Text>
							<ModalCloseButton />
						</ModalHeader>
						<ModalBody fontSize="lg" mt="4" textAlign="center">
							<Wrap justify="center" spacing="8px" spacingY="4">
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Nama Barang</FormLabel>
										<Input
											type="text"
											{...register('nama', { required: true })}
										/>
										{errors.nama && (
											<Text color="red.500">Nama barang harus diisi.</Text>
										)}
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Stock</FormLabel>
										<Input
											type="number"
											{...register('stock', { required: true })}
										/>
										{errors.stock && (
											<Text color="red.500">Stock harus diisi.</Text>
										)}
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Harga Beli</FormLabel>
										<Input
											type="number"
											{...register('harga_beli', { required: true })}
										/>
										{errors.harga_beli && (
											<Text color="red.500">Harga beli harus diisi.</Text>
										)}
									</FormControl>
								</WrapItem>
								<WrapItem>
									<FormControl w="340px">
										<FormLabel>Harga Jual</FormLabel>
										<Input
											type="number"
											{...register('harga_jual', { required: true })}
										/>
										{errors.harga_jual && (
											<Text color="red.500">Harga jual harus diisi.</Text>
										)}
									</FormControl>
								</WrapItem>
								<WrapItem>
									<WrapItem>
										<FormControl w="340px">
											<FormLabel textAlign="center">Foto</FormLabel>
											<Dropzone
												onDrop={(acceptedFiles) =>
													handleFileSelect(acceptedFiles)
												}
											>
												{({ getRootProps, getInputProps }) => (
													<Flex
														{...getRootProps()}
														direction="column"
														alignItems="center"
														p={4}
														borderWidth={2}
														borderRadius="lg"
														borderColor="gray.300"
														cursor="pointer"
													>
														<input {...getInputProps()} />
														<Icon
															as={BsCloudUploadFill}
															boxSize={8}
															color="gray.500"
														/>
														<Text mt={2} color="gray.600">
															{selectedFile ? selectedFile.name : 'Pilih file'}
														</Text>
													</Flex>
												)}
											</Dropzone>
											{errors.foto && (
												<Text color="red.500">Foto barang harus diupload.</Text>
											)}
										</FormControl>
									</WrapItem>
								</WrapItem>
							</Wrap>
						</ModalBody>
						<ModalFooter display="flex" justifyContent="center">
							<Button
								variant="ghost"
								mr={6}
								onClick={onClose}
								rounded="lg"
								bg="#fff"
								w="50%"
								h="50px"
								fontSize="lg"
								color="#3A8869"
								borderWidth="2px"
								borderColor="#3A8869"
							>
								Batal
							</Button>
							<Button
								type="submit"
								variant="ghost"
								mr={3}
								rounded="lg"
								bg="#3A8869"
								w="50%"
								h="50px"
								fontSize="lg"
								color="white"
								_hover={{ bg: '#3A8869' }}
								borderWidth="2px"
								borderColor="#3A8869"
								isLoading={isLoading}
								loadingText="Menyimpan..."
							>
								Simpan
							</Button>
						</ModalFooter>
					</form>
				</ModalContent>
			</Modal>
		</>
	);
};

export default ButtonPost;
