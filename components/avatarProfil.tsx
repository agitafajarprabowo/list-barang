import { Avatar, Flex, Stack, Text } from '@chakra-ui/react';
import * as React from 'react';

const AvatarProfil: React.FC = () => {
	return (
		<Flex align="center" borderBottomWidth="2px" mb="6" pb="6">
			<Avatar bg="teal.500" size="sm" mr="4" />
			<Stack fontSize="sm" spacing="-1">
				<Text>Admin</Text>
				<Text>Mr. Admin</Text>
			</Stack>
		</Flex>
	);
};

export default AvatarProfil;
