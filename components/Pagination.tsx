import { Box, ButtonGroup, Button, Text } from '@chakra-ui/react';
import React from 'react';

interface PaginationProps {
	itemsPerPage: number;
	totalItems: number;
	currentPage: number;
	paginate: (pageNumber: number) => void;
}

const Pagination: React.FC<PaginationProps> = ({
	itemsPerPage,
	totalItems,
	currentPage,
	paginate,
}) => {
	const pageNumbers = Math.ceil(totalItems / itemsPerPage);

	const handlePaginationClick = (pageNumber: number) => {
		paginate(pageNumber);
	};

	return (
		<Box
			mt="4"
			display="flex"
			justifyContent="space-between"
			alignItems="center"
		>
			<Text mx="4" fontSize="sm">
				Showing {itemsPerPage * (currentPage - 1) + 1} -{' '}
				{Math.min(itemsPerPage * currentPage, totalItems)} of {totalItems}
			</Text>
			<ButtonGroup isAttached variant="outline">
				{Array.from({ length: pageNumbers }, (_, index) => index + 1).map(
					(number) => (
						<Button
							key={number}
							size="md"
							onClick={() => handlePaginationClick(number)}
							colorScheme={number === currentPage ? 'green' : 'gray'}
						>
							{number}
						</Button>
					)
				)}
			</ButtonGroup>
		</Box>
	);
};

export default Pagination;
