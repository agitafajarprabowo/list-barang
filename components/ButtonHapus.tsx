import {
	Button,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
	useToast,
	Text,
	Box,
} from '@chakra-ui/react';
import * as React from 'react';
import { IoWarningOutline } from 'react-icons/io5';
import { BiTrash } from 'react-icons/bi';
import { useDeleteListBarang } from '../http/hooks/useDeleteListBarang';
import { useRefetchListBarang } from '../http/hooks/useRefetchListBarang';

interface DeleteListBarangProps {
	id: number;
	nama: string;
}

const ButtonHapus: React.FC<DeleteListBarangProps> = ({ id, nama }) => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const { refetchListBarangView } = useRefetchListBarang();
	const { mutateAsync: deleteListBarang, isLoading: isDeleting } =
		useDeleteListBarang();

	const handleDelete = async () => {
		await deleteListBarang(
			{ params: { id } },
			{
				onSuccess: async () => {
					await refetchListBarangView();
					onClose();
				},
			}
		);
	};

	return (
		<Box>
			<Box
				onClick={onOpen}
				bg="#E02B2B"
				display="flex"
				justifyItems="center"
				alignItems="center"
				rounded="xl"
				cursor="pointer"
			>
				<Icon color="#fff" as={BiTrash} w={4} h={4} m="2" />
			</Box>
			<Modal isOpen={isOpen} onClose={onClose} size="lg" isCentered>
				<ModalOverlay />
				<ModalContent px="12" py="2" rounded="xl">
					<ModalHeader
						justifyContent="center"
						display="flex"
						color="#E02B2B"
						mt="2"
					>
						<Icon as={IoWarningOutline} w="20" h="20" color="#E02B2B" />
					</ModalHeader>
					<ModalBody fontSize="lg" mb="10" mt="6" textAlign="center">
						<Text fontWeight="bold" fontSize="xl" mb="2">
							Konfirmasi
						</Text>
						<Text> Anda yakin akan menghapus {nama} ini ?</Text>
					</ModalBody>
					<ModalFooter display="flex" justifyContent="center">
						<Button
							variant="ghost"
							mr={6}
							onClick={onClose}
							rounded="lg"
							bg="#F4F4F4"
							w="50%"
							h="50px"
							fontSize="lg"
							color="#4A4A4A"
						>
							Batal
						</Button>
						<Button
							variant="ghost"
							mr={3}
							rounded="lg"
							bg="#E02B2B"
							w="50%"
							h="50px"
							fontSize="lg"
							color="white"
							_hover={{ bg: '#E02B2B' }}
							onClick={handleDelete}
							isLoading={isDeleting}
						>
							Delete
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</Box>
	);
};

export default ButtonHapus;
