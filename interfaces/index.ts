// You can include shared interfaces/types in a separate file
// and then use them in any component by importing them. For
// example, to import the interface below do:
//
// import { User } from 'path/to/interfaces';

export interface ListBarang {
	data?: any;
	id?: number;
	foto?: string;
	nama?: string;
	harga_jual?: string;
	harga_beli?: string;
	stock?: string;
}
