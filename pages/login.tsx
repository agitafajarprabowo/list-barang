import { useState } from 'react';
import axios from 'axios';
import { NextPageWithLayout } from './page';
import { useRouter } from 'next/router';
import {
	Box,
	Button,
	Grid,
	GridItem,
	Img,
	Input,
	Stack,
	useToast,
	Text,
	Icon,
} from '@chakra-ui/react';
import {
	getFailToastConfigs,
	getSuccessToastConfigs,
} from '../lib/toastConfigs';
import { IoIosChatbubbles } from 'react-icons/io';

const loginPage: NextPageWithLayout = () => {
	const router = useRouter();
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const toast = useToast();
	const handleLogin = async () => {
		try {
			const response = await axios.post('/api/login', { username, password });
			toast(
				getSuccessToastConfigs({
					title: 'Berhasil',
					description: 'Login',
				})
			);
			router.push('/');
		} catch (error) {
			console.error(error);
			toast(
				getFailToastConfigs({
					title: 'Gagal',
					description: 'Email dan Password anda salah',
				})
			);
		}
	};

	return (
		<Grid gridTemplateColumns="1fr 1fr" h="100vh">
			<GridItem display="flex" alignItems="center">
				<Stack w="full" px={32} spacing={3} textAlign="center" align="center">
					<Icon as={IoIosChatbubbles} w="12" h="12" color="#263137" />
					<Text fontSize="3xl" pb="12" fontWeight="bold" color="#263137">
						LOGIN
					</Text>
					<Input
						type="text"
						placeholder="Username"
						value={username}
						onChange={(e) => setUsername(e.target.value)}
					/>
					<Input
						type="password"
						placeholder="Password"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
					<Button
						bg="#3A8869"
						color="white"
						_hover={{ bg: '#3A8869', color: 'white' }}
						onClick={handleLogin}
						w="full"
					>
						Login
					</Button>
				</Stack>
			</GridItem>
			<GridItem bg="#3A8869" display="flex" alignItems="center">
				<Img src="/pic/asset-login.png" p={32} />
			</GridItem>
		</Grid>
	);
};

export default loginPage;
