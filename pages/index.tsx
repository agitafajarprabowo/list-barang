import {
	Box,
	Center,
	Flex,
	Grid,
	GridItem,
	Input,
	ListItem,
	Text,
} from '@chakra-ui/react';
import BaseLayout from '../components/layout';
import { NextPageWithLayout } from './page';
import ButtonPost from '../components/ButtonPost';
import SearchField from '../components/SearchField';
import ListProduct from '../components/ListProduct';
import { useGetAllListBarang } from '../http/hooks/useGetAllListBarang';
import { useRefetchListBarang } from '../http/hooks/useRefetchListBarang';
import { usePostListBarang } from '../http/hooks/usePostListBarang';
import React, { useEffect, useState } from 'react';
import Pagination from '../components/Pagination';

const IndexPage: NextPageWithLayout = () => {
	const { data: dataList } = useGetAllListBarang();
	const { mutateAsync: mutatePostListBarang } = usePostListBarang();
	const { refetchListBarangView } = useRefetchListBarang();
	const [filteredDataList, setFilteredDataList] = useState(dataList);
	const [currentPage, setCurrentPage] = useState(1);
	const [itemsPerPage] = useState(3);
	const [currentItems, setCurrentItems] = useState([]);

	const handleSearch = async (searchTerm: string) => {
		const filteredList = dataList.filter((item) =>
			item.nama.toLowerCase().includes(searchTerm.toLowerCase())
		);
		setFilteredDataList(filteredList);
		setCurrentPage(1);
		await refetchListBarangView();
	};

	const paginate = (pageNumber: number) => {
		setCurrentPage(pageNumber);
	};

	useEffect(() => {
		const indexOfLastItem = currentPage * itemsPerPage;
		const indexOfFirstItem = indexOfLastItem - itemsPerPage;
		const slicedItems = filteredDataList?.slice(
			indexOfFirstItem,
			indexOfLastItem
		);
		setCurrentItems(slicedItems);
	}, [currentPage, filteredDataList]);

	useEffect(() => {
		setFilteredDataList(dataList);
		setCurrentPage(1);
	}, [dataList]);

	return (
		<Box>
			<Text
				fontSize="2xl"
				fontWeight="bold"
				borderBottomWidth="2px"
				pb="2"
				mb="4"
			>
				List Barang
			</Text>
			<Flex justifyContent="space-between" mb="6">
				<ButtonPost
					mutateAsync={mutatePostListBarang}
					refetchAsync={refetchListBarangView}
				/>
				<SearchField onSearch={handleSearch} />
			</Flex>
			{currentItems?.length === 0 ? (
				<>
					<Center fontSize="3xl" fontWeight="bold" textAlign="center" mt="24">
						Belum ada list barang <br />
						silahkan isi terlebih dahulu
					</Center>
				</>
			) : (
				<>
					<Box bg="white" rounded="xl">
						<Grid
							gridTemplateColumns="min-content 1.2fr 1fr 1fr 1fr 1fr 0.8fr"
							p="4"
							borderWidth="2px"
							roundedTopLeft="xl"
							roundedTopRight="xl"
						>
							<GridItem mr="2">No</GridItem>
							<GridItem>Foto</GridItem>
							<GridItem textAlign="center">Nama</GridItem>
							<GridItem textAlign="center">Harga Jual</GridItem>
							<GridItem textAlign="center">Harga Beli</GridItem>
							<GridItem textAlign="center">Stock</GridItem>
							<GridItem textAlign="center">Aksi</GridItem>
						</Grid>
						<ListProduct dataList={currentItems} />
						<Pagination
							itemsPerPage={itemsPerPage}
							totalItems={filteredDataList?.length}
							currentPage={currentPage}
							paginate={paginate}
						/>
					</Box>
				</>
			)}
		</Box>
	);
};

IndexPage.getLayout = (page) => {
	return <BaseLayout pageTitle="Todolist Apps">{page}</BaseLayout>;
};

export default IndexPage;
