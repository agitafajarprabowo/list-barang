import { PrismaClient } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { ListBarang } from '../../../interfaces';

const prisma = new PrismaClient();

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const { method, query, body } = req;

	if (method === 'GET') {
		if (query.id) {
			try {
				const barangId = Number(query.id);
				const data: ListBarang | null = await prisma.post.findUnique({
					where: { id: barangId },
				});
				if (data) {
					res.status(200).json({ data });
				} else {
					res.status(404).json({ message: 'Barang not found' });
				}
			} catch (error) {
				console.error(error);
				res.status(500).json({ message: 'Internal server error' });
			}
		} else {
			try {
				const data: ListBarang[] = await prisma.post.findMany();
				res.status(200).json({ data });
			} catch (error) {
				console.error(error);
				res.status(500).json({ message: 'Internal server error' });
			}
		}
	} else if (method === 'POST') {
		try {
			const { foto, nama, harga_beli, harga_jual, stock } = body;

			// Modifikasi untuk mendapatkan nama file dari URL foto
			const fileName = foto.split('/').pop() || '';
			const createdBarang: ListBarang = await prisma.post.create({
				data: { foto: fileName, nama, harga_beli, harga_jual, stock },
			});
			res.status(201).json({ data: createdBarang });
		} catch (error) {
			console.error(error);
			res.status(500).json({ message: 'Internal server error' });
		}
	} else if (method === 'PUT') {
		try {
			if (query.id) {
				const barangId = Number(query.id);
				const { foto, nama, harga_beli, harga_jual, stock } = body;
				const updatedBarang: ListBarang = await prisma.post.update({
					where: { id: barangId },
					data: { foto, nama, harga_beli, harga_jual, stock },
				});
				res.status(200).json({ data: updatedBarang });
			} else {
				res.status(400).json({ message: 'Invalid request' });
			}
		} catch (error) {
			console.error(error);
			res.status(500).json({ message: 'Internal server error' });
		}
	} else if (method === 'DELETE') {
		try {
			if (query.id) {
				const barangId = Number(query.id);
				const deletedBarang: ListBarang = await prisma.post.delete({
					where: { id: barangId },
				});
				res.status(200).json({ barang: deletedBarang });
			} else {
				res.status(400).json({ message: 'Invalid request' });
			}
		} catch (error) {
			console.error(error);
			res.status(500).json({ message: 'Internal server error' });
		}
	} else {
		res.status(405).json({ message: 'Method Not Allowed' });
	}
}
