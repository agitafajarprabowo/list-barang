import { NextApiRequest, NextApiResponse } from 'next';
import { setCookie } from 'nookies';
import jwt from 'jsonwebtoken';

const handleLogin = (req: NextApiRequest, res: NextApiResponse) => {
	const { username, password } = req.body;

	if (username === 'admin1@admin.com' && password === 'admin123') {
		const token = jwt.sign({ username }, 'secret_key', { expiresIn: '30d' });

		setCookie({ res }, 'token', token, {
			maxAge: 30 * 24 * 60 * 60,
		});

		res.status(200).json({ success: true, message: 'Login successful' });
	} else {
		res.status(401).json({ success: false, message: 'Invalid credentials' });
	}
};

export default handleLogin;
