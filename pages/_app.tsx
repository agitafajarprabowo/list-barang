import '../styles/global.css';
import { ChakraBaseProvider, extendTheme, useToast } from '@chakra-ui/react';
import {
	Hydrate,
	QueryClient,
	QueryClientProvider,
} from '@tanstack/react-query';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies';
import React, { useEffect } from 'react';
import { getWarningToastConfigs } from '../lib/toastConfigs';

export const theme = extendTheme({
	config: {
		initialColorMode: 'light',
		useSystemColorMode: false,
	},
});

const queryClient = new QueryClient();

export default function App({ Component, pageProps }) {
	const getLayout = Component.getLayout || ((page) => page);
	const router = useRouter();
	const toast = useToast();

	useEffect(() => {
		const cookies = parseCookies();
		const token = cookies.token;

		if (!token && router.pathname !== '/login') {
			router.replace('/login');
			toast(
				getWarningToastConfigs({
					title: 'Gagal',
					description: 'Silahkan Login Terlebih Dahulu',
				})
			);
		}
	}, []);

	return (
		<QueryClientProvider client={queryClient}>
			<Hydrate state={pageProps.dehydratedState}>
				<ChakraBaseProvider theme={theme}>
					{getLayout(<Component {...pageProps} />)}
				</ChakraBaseProvider>
			</Hydrate>
		</QueryClientProvider>
	);
}
