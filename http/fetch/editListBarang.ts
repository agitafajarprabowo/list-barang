import axios from 'axios';
import { ListBarang } from '../../interfaces';

export interface EditListBarangArgs {
	data: ListBarang;
}

export const editListBarang = async (data) => {
	try {
		const response = await axios.put<ListBarang>(`api/products?id=${data.id}`, {
			data,
		});
		return response.data;
	} catch (error) {
		throw new Error('Gagal menyimpan data');
	}
};
