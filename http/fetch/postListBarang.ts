import axios from 'axios';
import { ListBarang } from '../../interfaces';

export interface PostListBarangArgs {
	data: ListBarang;
}

export const PostListBarang = async (args: PostListBarangArgs) => {
	const { data } = args;

	try {
		const response = await axios.post('/api/products', data);
		console.log('responpost', response);
		return response.data;
	} catch (error) {
		throw new Error('Failed to post list barang');
	}
};
