import axios from 'axios';
import { ListBarang } from '../../interfaces';

export interface DeleteListBarangParams {
	id?: number;
}

export interface DeleteListBarangArgs {
	params: DeleteListBarangParams;
}

export const deleteListBarang = async (args: DeleteListBarangArgs) => {
	const params = {
		...args.params,
	};

	delete params.id;

	const response = await axios.delete<ListBarang>(
		`api/products?id=${args.params.id}`,
		{
			params,
		}
	);

	return response.data || [];
};
