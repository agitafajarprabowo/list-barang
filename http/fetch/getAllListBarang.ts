import axios from 'axios';
import { ListBarang } from '../../interfaces';

export const getAllListBarang = async (): Promise<ListBarang[]> => {
	const response = await axios.get('api/products');
	return response.data.data || [];
};
