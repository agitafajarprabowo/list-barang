import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import {
	DeleteListBarangArgs,
	deleteListBarang,
} from '../fetch/deleteListBarang';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../lib/toastConfigs';

export const useDeleteListBarang = () => {
	const toast = useToast();

	const mutation = useMutation<unknown, AxiosError, DeleteListBarangArgs>(
		(args) => {
			return deleteListBarang({
				...args,
			});
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Berhasil',
						description: 'Data berhasil dihapus',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'Gagal',
						description: 'Data gagal untuk dihapus',
					})
				);
			},
		}
	);

	return { ...mutation };
};
