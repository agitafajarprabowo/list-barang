import { useGetAllListBarang } from './useGetAllListBarang';

export const useRefetchListBarang = () => {
	const { data: listBarang, refetch: refetchListBarangView } =
		useGetAllListBarang();
	const listBarangId = listBarang?.find((item) => item.id);

	return {
		listBarangId,
		refetchListBarangView,
	};
};
