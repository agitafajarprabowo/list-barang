import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../lib/toastConfigs';
import { EditListBarangArgs, editListBarang } from '../fetch/editListBarang';
import { ListBarang } from '../../interfaces';

export const useEditListBarang = () => {
	const toast = useToast();

	const editListBarangMutation = useMutation<
		ListBarang | undefined,
		AxiosError,
		EditListBarangArgs
	>(
		(data) => {
			return editListBarang(data.data.id);
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Berhasil',
						description: 'Data berhasil disimpan',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'Gagal',
						description: 'Data gagal untuk disimpan',
					})
				);
			},
		}
	);

	return {
		editListBarangMutation,
	};
};
