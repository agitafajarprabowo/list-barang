import { useQuery } from '@tanstack/react-query';
import { getAllListBarang } from '../fetch/getAllListBarang';

export const useGetAllListBarang = () => {
	return useQuery(['list-barang'], () => getAllListBarang(), {
		keepPreviousData: true,
	});
};
