import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { PostListBarang, PostListBarangArgs } from '../fetch/postListBarang';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../lib/toastConfigs';

export const usePostListBarang = () => {
	const toast = useToast();

	return useMutation<string, AxiosError, PostListBarangArgs['data']>(
		(data) => {
			return PostListBarang({ data });
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Sukses',
						description: 'Barang berhasil disimpan',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'gagal',
						description: 'Barang gagal disimpan',
					})
				);
			},
		}
	);
};
