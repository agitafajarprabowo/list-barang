import { AxiosError } from 'axios';

export interface Response<
	R = Record<string, string>,
	M = Record<string, string>
> {
	data?: R;
	meta?: M;
}

export type AxiosErrorResponse = AxiosError;
